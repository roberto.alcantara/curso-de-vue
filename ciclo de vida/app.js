const app=new Vue({
    el: '#app',
    data:{
        saludo:'ciclo de vida'
    },
    beforeCreate(){
        console.log('beforeCreate');
    },
    created(){
        //al crear los metods, obseavdore y eventos, pero aun no accede al dom
        //aun no se puede acceder a el
        console.log('created');
    },
    beforeMount(){
        //se ejecuta antes de insertar el dom
        console.log('beforeMount');
    },
    mounted(){
        //se ejecuta al insertar el dom
        console.log('mounted');
    },
    beforeUpdate(){
        //al detectar cambios
        console.log('beforeUpdate');
    },
    update(){
        //al realizar los cambios
        console.log('update');
    },
    beforeDestroy(){
        //antes de destruir la instancia
        console.log('beforeDestroy');
    },
    destroyed(){
        //se destruye toda la insatancia         
        console.log('destroyed');
    },
    methods:({
        destruir(){
            this.$destroy();
        }
    })
});