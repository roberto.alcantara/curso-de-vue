const app =new Vue({
    el:'#app',
    data: {
        titulo:'Hola mundo con Vue',
        frutas:[{nombre:'Pera',cantidad:0},
                {nombre:'Menzana',cantidad:10},
                {nombre:'Platano',cantidad:15}
                ],
        nuevafruta:'',
        total:0        
    },
    methods:{
        agregarfruta(){
          this.frutas.push({
              nombre:this.nuevafruta, cantidad:0
          });
          this.nuevafruta='';
        }
    },
    computed:{
        sumarfrutas(){
            this.total=0;
            for(fruta of this.frutas){
                this.total=this.total + fruta.cantidad;
            }
            return this.total;
        }
    }

})